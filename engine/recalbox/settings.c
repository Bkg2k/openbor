#include "settings.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <openbor.h>

static bool LoadIntLine(char* line, const char* key, int* value)
{
  int l = (int)strlen(key);
  if (strncmp(line, key, l) == 0)
    if (line[l] == '=')
    {
      *value = (int)strtol(line + l + 1, NULL, 10);
      return true;
    }

  return false;
}

static bool LoadFloatLine(char* line, const char* key, float* value)
{
  int l = (int)strlen(key);
  if (strncmp(line, key, l) == 0)
    if (line[l] == '=')
    {
      *value = strtof(line + l + 1, NULL);
      return true;
    }

  return false;
}

static void SaveIntLine(FILE* file, const char* key, int value)
{
  char buffer[2048];
  snprintf(buffer, sizeof(buffer), "%s=%d\n", key, value);
  fputs(buffer, file);
}

static void SaveFloatLine(FILE* file, const char* key, float value)
{
  char buffer[2048];
  snprintf(buffer, sizeof(buffer), "%s=%f\n", key, value);
  fputs(buffer, file);
}

bool StartsWith(const char *str, const char *base)
{
  return strncmp(str, base, strlen(base)) == 0;
}

void serialize_config(FILE* tofile)
{
  SaveIntLine(tofile, "CompatibleVersion", (int)savedata.compatibleversion);
  SaveIntLine(tofile, "Gamma", savedata.gamma);
  SaveIntLine(tofile, "Brightness", savedata.brightness);
  SaveIntLine(tofile, "SoundVolume", savedata.soundvol);
  SaveIntLine(tofile, "PlayMusic", savedata.usemusic);
  SaveIntLine(tofile, "MusicVolume", savedata.musicvol);
  SaveIntLine(tofile, "EffectVolume", savedata.effectvol);
  SaveIntLine(tofile, "UseJoystick", savedata.usejoy);
  SaveIntLine(tofile, "Mode", savedata.mode);
  SaveIntLine(tofile, "WindowPosition", savedata.windowpos);
  SaveIntLine(tofile, "ShowTitles", savedata.showtitles);
  SaveIntLine(tofile, "NTSC", savedata.videoNTSC);
  SaveIntLine(tofile, "SoftwareFilter", savedata.swfilter);
  SaveIntLine(tofile, "Logo", savedata.logo);
  SaveIntLine(tofile, "UseLogs", savedata.uselog);
  SaveIntLine(tofile, "DebugInfo", savedata.debuginfo);
  SaveIntLine(tofile, "FullScreen", savedata.fullscreen);
  SaveIntLine(tofile, "PixelPefect", savedata.stretch ? 0 : 1);
  SaveIntLine(tofile, "VSync", savedata.vsync);

  for(int i=MAX_PLAYERS; --i >= 0;)
  {
    char keyname[2048];
    snprintf(keyname, sizeof(keyname), "JoystickRumble-P%d", i);            SaveIntLine(tofile, keyname, savedata.joyrumble[i]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK", i);     SaveIntLine(tofile, keyname, savedata.keys[i][SDID_ATTACK]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK2", i);    SaveIntLine(tofile, keyname, savedata.keys[i][SDID_ATTACK2]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK3", i);    SaveIntLine(tofile, keyname, savedata.keys[i][SDID_ATTACK3]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK4", i);    SaveIntLine(tofile, keyname, savedata.keys[i][SDID_ATTACK4]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-HOTKEY", i);     SaveIntLine(tofile, keyname, savedata.keys[i][SDID_ESC]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-JUMP", i);       SaveIntLine(tofile, keyname, savedata.keys[i][SDID_JUMP]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVEUP", i);     SaveIntLine(tofile, keyname, savedata.keys[i][SDID_MOVEUP]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVEDOWN", i);   SaveIntLine(tofile, keyname, savedata.keys[i][SDID_MOVEDOWN]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVELEFT", i);   SaveIntLine(tofile, keyname, savedata.keys[i][SDID_MOVELEFT]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVERIGHT", i);  SaveIntLine(tofile, keyname, savedata.keys[i][SDID_MOVERIGHT]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-SCREENSHOT", i); SaveIntLine(tofile, keyname, savedata.keys[i][SDID_SCREENSHOT]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-SPECIAL", i);    SaveIntLine(tofile, keyname, savedata.keys[i][SDID_SPECIAL]);
    snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-START", i);      SaveIntLine(tofile, keyname, savedata.keys[i][SDID_START]);
  }

  #if SDL
  SaveIntLine(tofile, "UseOpenGL", savedata.usegl);
  SaveIntLine(tofile, "HardwareFilter", savedata.hwfilter);
  SaveFloatLine(tofile, "HardwareScale", savedata.hwscale);
  #endif
}

void deserialize_config(FILE* fromfile)
{
  char Line[2048];

  clearsettings();

  while(fgets(Line, sizeof(Line), fromfile) != NULL)
  {
    int version;
    if (LoadIntLine(Line, "CompatibleVersion", &version))
    {
      savedata.compatibleversion = (int)version;
      continue;
    }
    if (LoadIntLine(Line, "Gamma", &savedata.gamma)) continue;
    if (LoadIntLine(Line, "Brightness", &savedata.brightness)) continue;
    if (LoadIntLine(Line, "SoundVolume", &savedata.soundvol)) continue;
    if (LoadIntLine(Line, "PlayMusic", &savedata.usemusic)) continue;
    if (LoadIntLine(Line, "MusicVolume", &savedata.musicvol)) continue;
    if (LoadIntLine(Line, "EffectVolume", &savedata.effectvol)) continue;
    if (LoadIntLine(Line, "UseJoystick", &savedata.usejoy)) continue;
    if (LoadIntLine(Line, "Mode", &savedata.mode)) continue;
    if (LoadIntLine(Line, "WindowPosition", &savedata.windowpos)) continue;
    if (LoadIntLine(Line, "ShowTitles", &savedata.showtitles)) continue;
    if (LoadIntLine(Line, "NTSC", &savedata.videoNTSC)) continue;
    if (LoadIntLine(Line, "SoftwareFilter", &savedata.swfilter)) continue;
    if (LoadIntLine(Line, "Logo", &savedata.logo)) continue;
    if (LoadIntLine(Line, "UseLogs", &savedata.uselog)) continue;
    if (LoadIntLine(Line, "DebugInfo", &savedata.debuginfo)) continue;
    if (LoadIntLine(Line, "FullScreen", &savedata.fullscreen)) continue;
    if (LoadIntLine(Line, "PixelPefect", &savedata.stretch))
    {
      savedata.stretch = savedata.stretch ? 0 : 1; // Inverse
      continue;
    }
    if (LoadIntLine(Line, "VSync", &savedata.vsync)) continue;

    if (StartsWith(Line, "Joystick"))
    for(int i=MAX_PLAYERS; --i >= 0;)
    {
      char keyname[2048];
      snprintf(keyname, sizeof(keyname), "JoystickRumble-P%d", i);
      if (LoadIntLine(Line, keyname, &savedata.joyrumble[i])) continue;
      if (StartsWith(Line, "JoystickButton"))
      {
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK", i);     LoadIntLine(Line, keyname, &savedata.keys[i][SDID_ATTACK]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK2", i);    LoadIntLine(Line, keyname, &savedata.keys[i][SDID_ATTACK2]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK3", i);    LoadIntLine(Line, keyname, &savedata.keys[i][SDID_ATTACK3]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-ATTACK4", i);    LoadIntLine(Line, keyname, &savedata.keys[i][SDID_ATTACK4]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-HOTKEY", i);     LoadIntLine(Line, keyname, &savedata.keys[i][SDID_ESC]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-JUMP", i);       LoadIntLine(Line, keyname, &savedata.keys[i][SDID_JUMP]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVEUP", i);     LoadIntLine(Line, keyname, &savedata.keys[i][SDID_MOVEUP]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVEDOWN", i);   LoadIntLine(Line, keyname, &savedata.keys[i][SDID_MOVEDOWN]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVELEFT", i);   LoadIntLine(Line, keyname, &savedata.keys[i][SDID_MOVELEFT]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-MOVERIGHT", i);  LoadIntLine(Line, keyname, &savedata.keys[i][SDID_MOVERIGHT]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-SCREENSHOT", i); LoadIntLine(Line, keyname, &savedata.keys[i][SDID_SCREENSHOT]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-SPECIAL", i);    LoadIntLine(Line, keyname, &savedata.keys[i][SDID_SPECIAL]);
        snprintf(keyname, sizeof(keyname), "JoystickButton-P%d-START", i);      LoadIntLine(Line, keyname, &savedata.keys[i][SDID_START]);
      }
    }

    #if SDL
      if (LoadIntLine(Line, "UseOpenGL", &savedata.usegl)) continue;
      if (LoadIntLine(Line, "HardwareFilter", &savedata.hwfilter)) continue;
      if (LoadFloatLine(Line, "HardwareScale", &savedata.hwscale)) continue;
    #endif
  }
}
