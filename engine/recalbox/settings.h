#ifndef OPENBOR_RECALBOX_SETTINGS_H
#define OPENBOR_RECALBOX_SETTINGS_H

#include "source/savedata.h"
#include "stdio.h"

void serialize_config(FILE* tofile);

void deserialize_config(FILE* fromfile);

#endif //OPENBOR_RECALBOX_SETTINGS_H
